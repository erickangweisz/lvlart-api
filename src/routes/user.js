import Router from 'express'
import * as userController from '../controllers/user'
import { ensureAuth } from '../middlewares/authenticated'

const router = new Router()
const multipart = require('connect-multiparty')
const md_upload_header = multipart({ uploadDir: './uploads/headers' })
const md_upload_avatar = multipart({ uploadDir: './uploads/avatars' })

router.post('/register', userController.createUser)
router.post('/login', userController.loginUser)
router.post('/check-username', userController.checkUsernameIsRegistered)
router.post('/check-email', userController.checkEmailIsRegistered)
router.post('/check-birthday', userController.checkBirthday)
router.post('/recover-password', userController.recoverPassword)

router.post('/upload-header/:id', [ensureAuth, md_upload_header], userController.uploadImageHeader)
router.post('/upload-avatar/:id', [ensureAuth, md_upload_avatar], userController.uploadImageAvatar)
router.get('/get-header/:img_header', userController.getImageHeader)
router.get('/get-avatar/:avatar', userController.getImageAvatar)

router.get('/users/:page?', ensureAuth, userController.getUsers)
router.get('/user/:id', ensureAuth, userController.getUser)

router.put('/update-user/:id', ensureAuth, userController.updateUser)
router.put('/change-password/:id', ensureAuth, userController.changePassword)
//router.put('/update-state/:id', ensureAuth, userController.updateState)
router.put('/set-active-status/:id', ensureAuth, userController.setActiveStatus)
router.put('/set-active-socialnetworks/:id', ensureAuth, userController.setStateOfSocialNetworkButtons)

//router.delete('/delete-user/:id', ensureAuth, userController.deleteUser)

export default router