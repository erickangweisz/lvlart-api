import { Router } from 'express'
import userRoutes from './user'

const router = Router()
const lvlartApiPath = '/lvlart-api'

router.use(lvlartApiPath, userRoutes)

export default router