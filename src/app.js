import express from 'express'
import bodyParser from 'body-parser'
import routes from './routes/index.js'
import path from 'path'

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// config http headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Allow-Request-Method')
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS')
    next()
})

// images categories
app.get('/lvlart-api/images/user-illustrator', (req, res) => {
    res.sendFile(path.join(__dirname, '../images', 'user-illustrator.gif'))
})
app.get('/lvlart-api/images/user-photography', (req, res) => {
    res.sendFile(path.join(__dirname, '../images', 'user-photography.gif'))
})
app.get('/lvlart-api/images/user-watcher', (req, res) => {
    res.sendFile(path.join(__dirname, '../images', 'user-watcher.gif'))
})

// base path
app.use(routes)

export default app