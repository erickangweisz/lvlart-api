import mongoose from 'mongoose'
import chalk from 'chalk'
import app from './app'
import config from './config'

const apiIp = config.api.ip
const apiPort = config.api.port
const dbIp = config.db.ip
const dbPort = config.db.port
const dbDatabase = config.db.database

mongoose.connect(`mongodb://${dbIp}:${dbPort}/${dbDatabase}`, { useNewUrlParser: true }) 
    .then(() => {
        console.log('Connected to MONGODB')
    })
    .catch(err => {
        console.log(err)
        process.exit(1)
    })
mongoose.set('useCreateIndex', true)

app.listen(apiPort, () => {
    console.log('\nMODE ->', chalk.bgCyan(config.mode))
    console.log(`API lvl-art listen on http://${apiIp}:${apiPort}\n`)
})