import fs from 'fs'
import path from 'path'
import bcrypt from 'bcrypt-nodejs'
import * as jwt from '../services/jwt'
import User from '../models/user'

export function createUser(req, res) {
    const user = new User()

    if (req.body.password) {
        bcrypt.hash(req.body.password, null, null, (err, hash) => {
            user.password = hash
        })
    } else res.status(200).send({ message: 'Enter password' })

    user.active = true
    user.role = 'ROLE_USER'
    user.username = req.body.username
    user.firstname = req.body.firstname
    user.lastname = req.body.lastname
    user.state = 'Put your state here!'
    user.birthday = req.body.birthday
    user.email = req.body.email
    user.category = req.body.category
    user.signup_date = new Date()
    user.score = 0
    user.visits = 0
    user.experience = 0
    user.n_challenges = 0
    user.n_victories = 0
    user.n_defeats = 0
    user.profile_facebook = null
    user.profile_twitter = null
    user.profile_deviantart = null
    user.active_facebook = true
    user.active_twitter = true
    user.active_deviantart = true
    user.last_visits = null
    user.avatar = null
    user.img_head = null

    user.save((err) => {
        if (err) res.status(500).send({ message: `error creating the user: ${err}` })
        res.status(201).send({ token: jwt.createToken(user) })
    })
}

export function loginUser(req, res) {
    const params = req.body
    const email = params['email']
    const password = params['password']

    User.findOne({ email: email.toLowerCase() }, (err, user) => {
        if (err) res.status(500).send({ message: `Server error | ${err}` })
        else {
            if (!user) res.status(404).send({ message: 'User does not exist' })
            else {
                // check password
                bcrypt.compare(password, user.password, (err, check) => {
                    if (check) {
                        // return a jwt token
                        res.status(200).send({
                            token: jwt.createToken(user),
                            user: user
                        })
                    } else res.status(404).send({ message: 'The user could not log in' })
                })
            }
        }
    })
}

export function getUsers(req, res) {
    User.find().exec((err, users) => {
        if (err) res.status(500).send({ message: `request error: ${err}` })
        else {
            if (!users) res.status(404).send({ message: 'users do not exist' })
            else res.status(200).send({ users: users })
        }
    })
}

export function getUser(req, res) {
    const userId = req.params.id

    User.findById(userId, (err, user) => {
        if (err) res.status(500).send({ message: `Server error: ${err}` })
        else {
            if (!user) res.status(404).send({ message: `The user doesnt exist` })
            else res.status(200).send({ user })
        }
    })
}

export function updateUser(req, res) {
    const userId = req.params.id
    const update = req.body

    if (userId !== null && update !== null) {
        User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
            if (err) res.status(500).send({ message: `Server error | ${err}` })
            else {
                if (!userUpdated) res.status(404).send({ message: 'Failed to update user' })
                else res.status(200).send({ user: userUpdated })
            }
        })
    }
}

export function deleteUser(req, res) {
    const userId = req.params.userId

    User.findByIdAndRemove(userId, (err, user) => {
        if (err) res.status(500).send({ message: `request error: ${err}` })
        if (user) res.status(200).send({ message: 'The user has been deconsted' })
    })
}

export function checkUsernameIsRegistered(req, res) {
    const params = req.body
    const username = params.body

    User.findOne({ username: username }, (err, username) => {
        if (err) res.status(500).send({ message: `request error: ${err}` })
        if (!username) res.status(200).send({ username: false })
        if (username) res.status(200).send({ username: true })
    })
}

export function checkEmailIsRegistered(req, res) {
    const params = req.body
    const email = params.body

    User.findOne({ email: email }, (err, email) => {
        if (err) res.status(500).send({ message: `request error: ${err}` })
        if (!email) res.status(200).send({ email: false })
        if (email) res.status(200).send({ email: true })
    })
}

export function checkBirthday(req, res) {
    const params = req.body
    const birthday = params.body
    const year = Number(birthday.substring(0, 4))

    if (year < 1919 || year > 2010) 
        res.status(200).send({ birthday: true })
    else
        res.status(200).send({ birthday: false })
}

export function uploadImageHeader(req, res) {
    const userId = req.params.id

    if (req.files) {
        const file_path = req.files.img_head.path
        const file_split = file_path.split('/')
        const file_name = file_split[2]

        const ext_split = file_name.split('.')
        const file_ext = ext_split[1] 

        if (file_ext == 'png' || file_ext == 'jpg' ||
            file_ext == 'jpeg' || file_ext == 'gif') {
                User.findByIdAndUpdate(userId, { img_head: file_name }, (err, userUpdated) => {
                    if (!userUpdated) {
                        res.status(404).send({ message: 'the user has not been updated' })
                    } else {
                        res.status(200).send({ img_head: file_name, user: userUpdated })
                    }
                })
            }
    } else {
        res.status(200).send({ message: 'image not upload' })
    }
}

export function uploadImageAvatar(req, res) {
    const userId = req.params.id

    if (req.files) {
        const file_path = req.files.avatar.path
        const file_split = file_path.split('/')
        const file_name = file_split[2]

        const ext_split = file_name.split('.')
        const file_ext = ext_split[1]

        if (file_ext == 'png' || file_ext == 'jpg' ||
            file_ext == 'jpeg' || file_ext == 'gif') {
            User.findByIdAndUpdate(userId, { avatar: file_name }, (err, userUpdated) => {
                if (!userUpdated) {
                    res.status(404).send({ message: 'the user has not been updated' })
                } else {
                    res.status(200).send({ avatar: file_name, user: userUpdated })
                }
            })
        }
    } else {
        res.status(200).send({ message: 'image not upload' })
    }
}

export function changePassword(req, res) {
    const userId = req.user.sub
    const currentPassword = req.body.currentPassword
    const newPassword = req.body.newPassword
    let passwordHashed = ''

    /* check current password */
    if (currentPassword !== null && newPassword !== null) {
        User.findOne({ _id: userId }, (err, user) => {
            if (err) res.status(500).send({ message: `server error :: ${err}` })
            if (user) {
                bcrypt.compare(currentPassword, user.password, (err, check) => {
                    if (check) {
                        bcrypt.hash(newPassword, null, null, (err, hash) => {
                            passwordHashed = hash

                            if (userId !== null && passwordHashed !== null) {
                                User.findByIdAndUpdate(userId, { password: passwordHashed }, (err, userUpdated) => {
                                    if (err)
                                        res.status(500).send({ mesage: `Server error | ${err}` })
                                    else
                                        res.status(200).send({ userUpdated: userUpdated })
                                })
                            }
                        })
                    } else res.status(404).send({ message: `current password is not correct` })
                })
            }
        })
    }
    /* END check current password */    
}

export function setActiveStatus(req, res) {
    const userId = req.body.userId
    const active = req.body.active

    if (userId !== null && active !== null) {
        User.findOneAndUpdate({ _id: userId }, { active: active }, (err, userUpdated) => {
            if (err) res.status(500).send({ message: `server error :: ${err}` })
            if (userUpdated) res.status(200).send({ userUpdated: userUpdated })
        })
    }
}

export function setStateOfSocialNetworkButtons(req, res) {
    const userId = req.body.userId
    if (userId !== null || userId !== 'undefined') {
        if (req.body.hasOwnProperty('active_facebook')) {
            const active_facebook = req.body.active_facebook
            User.findOneAndUpdate({ _id: userId }, { active_facebook: active_facebook }, (err, userUpdated) => {
                if (err) res.status(500).send({ message: `server error :: ${err}` })
                if (userUpdated) res.status(200).send({ userUpdated: userUpdated })
            })
        } else if (req.body.hasOwnProperty('active_twitter')) {
            const active_twitter = req.body.active_twitter
            User.findOneAndUpdate({ _id: userId }, { active_twitter: active_twitter }, (err, userUpdated) => {
                if (err) res.status(500).send({ message: `server error :: ${err}` })
                if (userUpdated) res.status(200).send({ userUpdated: userUpdated })
            })
        } else if (req.body.hasOwnProperty('active_deviantart')) {
            const active_deviantart = req.body.active_deviantart
            User.findOneAndUpdate({ _id: userId }, { active_deviantart: active_deviantart }, (err, userUpdated) => {
                if (err) res.status(500).send({ message: `server error :: ${err}` })
                if (userUpdated) res.status(200).send({ userUpdated: userUpdated })
            })
        } else {
            res.status(500).send({ message: `server error :: ${err}` })
        }
    }
}

export function getImageHeader(req, res) {
    const imageHeader = req.params.img_header
    const path_file = './uploads/headers/' + imageHeader

    fs.exists(path_file, (exists) => {
        if (exists) res.sendFile(path.resolve(path_file))
        else res.status(200).send({ message: 'The image do not exist' })
    })
}

export function getImageAvatar(req, res) {
    const avatar = req.params.avatar
    const path_file = './uploads/avatars/' + avatar

    fs.exists(path_file, (exists) => {
        if (exists) res.sendFile(path.resolve(path_file))
        else res.status(200).send({ message: 'The image do not exist' })
    })
}

// TODO: pending development
export function recoverPassword(req, res) {
    const params = req.body
    const email = params.body
    console.log(email)
    res.status(200).send({ data: true })
}