'use strict'

const Image = require('../models/image')
const config = require('../config')
const multer = require('multer')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'gallery/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + '.png')
    }
})
const upload = multer({ storage: storage }).single('file')

export function getImage(req, res) {
    const imageId = req.params.imageId

    Image.findById(imageId, (err, image) => {
        if (err) res.status(500).send({ message: `request error: ${err}` })
        if (!image) res.status(404).send({ message: `image does not exist` })

        res.status(200).send({ image })
    })
}

export function getImages(req, res) {
    Image.find({}, (err, images) => {
        if (err) res.status(500).send({ message: `request error ${err}` })
        if (!images) res.status(404).send({ message: `images does not exist` })

        res.status(200).send({ images })
    })
}

export function saveImage(req, res) {
    upload(req, res, (err) => {
        if (err) {
            res.json({ error_code: 1, err_desc: err })
            return
        }
        let image = new Image()
        image.user_id = req.body.user_id
        image.title = req.body.title
        image.description = req.body.description
        image.url_image = config.DIRNAME + req.file.path('\\', '/')
        image.category = req.body.category
        image.n_likes = 0
        image.n_dislikes = 0
        image.uploaded_at = new Date()
        image.score = 0

        image.save((err, imageStored) => {
            if (err) res.status(500).send({ message: `error saving to database: ${err}` })

            res.status(201).send({ image: imageStored })
        })
        res.json({ error_code: 0, err_desc: null })
    })
}