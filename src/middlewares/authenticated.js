import jwt from 'jwt-simple'
import moment from 'moment'
import config from '../config'

export function ensureAuth(req, res, next) {
    if (!req.headers.authorization)
    return res.status(403).send({ message: 'The request does not have the authentication header' })

    var token = req.headers.authorization.replace(/['"]+/g, '')

    try {
        var payload = jwt.decode(token, config.privateKey)
        if (payload.exp <= moment().unix())
            return res.status(401).send({ message: 'Token has expired' })
    } catch (ex) {
        return res.status(404).send({ message: 'Token is invalid' })
    }
    req.user = payload
    next()
}