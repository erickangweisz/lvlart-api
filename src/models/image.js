'use strict'

import mongoose, { mongo } from 'mongoose'
const Schema = mongoose.Schema

const ImageSchema = Schema({
    title: String,
    description: String,
    url_image: String,
    user_id: String,
    category: { type: String, enum: ['Illustration', 'Photography', 'Watcher'] },
    n_likes: Number,
    n_dislikes: Number,
    uploaded_at: Date,
    score: Number
})
module.exports = mongoose.model('Image', ImageSchema)