import mongoose from 'mongoose'
const Schema = mongoose.Schema

const UserSchema = new Schema({
    active: Boolean,
    role: String,
    username: { type: String, unique: true, lowercase: true, required : true },
    password: { type: String, select: true },
    firstname: String,
    lastname: String,
    state: String,
    birthday: Date,
    avatar: String,
    score: Number,
    visits: Number,
    email: { type: String, unique: true, lowercase: true, required : true },
    signup_date: { type: Date, default: Date.now() },
    category: { type: String, enum: ['Illustration', 'Photography', 'Watcher'] },
    experience: Number,
    n_challenges: Number,
    n_victories: Number,
    n_defeats: Number,
    profile_facebook: String,
    profile_twitter: String,
    profile_deviantart: String,
    active_facebook: String,
    active_twitter: String,
    active_deviantart: String,
    last_visits: Array,
    img_head: String,
    last_login: Date,
    active: Boolean
})

export default mongoose.model('User', UserSchema)