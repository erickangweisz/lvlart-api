FROM node:10-alpine as builder

WORKDIR /app
COPY ./package.json ./package.json
RUN npm install

COPY . .

RUN npm run build

FROM node:10-alpine

WORKDIR /app

ENV NODE_ENV production

COPY ./package.json ./package.json

RUN npm install

COPY --from=builder /app/build ./build

CMD node build/index.js